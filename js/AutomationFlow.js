class AutomationFlow{
	constructor(name){
		this.flowName = name;
	}

	get name(){
		return this.flowName;
	}

}

module.exports = AutomationFlow;