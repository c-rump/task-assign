$(function(){

    $('body').on('click', '.btn-circle-inactive', function(){
        $('.btn-circle-active').removeClass('btn-circle-active').addClass('btn-circle-inactive');
        $(this).removeClass('btn-circle-inactive').addClass('btn-circle-active');
        $('.content-container').addClass('d-none');
        $('.content-container').removeClass('d-flex');

        var selected = $(this).next().text();
        if(selected == 'Users'){
            $('#usersContainer').css('opacity', 0);
            $('#usersContainer').removeClass('d-none');
            animateChangeDiv($('#usersContainer'));
        }
        else if(selected == 'Groups'){
            $('#groupsContainer').css('opacity', 0);
            $('#groupsContainer').removeClass('d-none');
            animateChangeDiv($('#groupsContainer'));
        }
    });

    $('body').on('mouseover', '.item-close-x', function(){
        $(this).parent().addClass('x-active');
    }).on('mouseout', '.item-close-x', function(){
        $(this).parent().removeClass('x-active');
    });

    $('body').on('click', '.item-close-x', function(){
        username = $(this).closest('tr').attr('data-user');
        groupname = $(this).parent().children().first().text();
        $('#removeGroupUser').text(username);
        $('#removeGroupGroup').text(groupname);
        userid = $(this).closest('tr').attr('data-id');
        groupid = $(this).attr('data-id');
        removeButtonId = this.id;

        $('#removeFromGroupModal').modal('show');
    });

    $('#removeFromGroupConfirm').click(function(){
        var json = {orgid:localStorage.orgid, groupid:groupid, userid:userid}

        $.ajax({
            type: 'POST',
            url: "http://127.0.0.1:8085/removeUserFromGroup",
            contentType: 'application/json; charset=utf-8',
            dataType: 'JSON',
            data: JSON.stringify(json),
            success: function(response){
                $('#' + removeButtonId).parent().remove();
            },
            error: function (xhr, textStatus, errorThrown){
                console.log('error' + errorThrown);
                console.log('xhr', xhr);
                console.log(xhr.responseText);
            }
        });

        $('#removeFromGroupModal').modal('hide');
    });

    $('body').on('mouseenter', '.user-tr', function(){
        $(this).find('.d-none').removeClass('d-none');
        $(this).find('.row-close-x').html('&times;');
    }).on('mouseleave', '.user-tr', function(){
        $(this).find('.item-close-x').addClass('d-none');
        $(this).find('.row-close-x').html('<i class="fas fa-user"></i>');
        $(this).find('.col-auto').addClass('d-none');
        $(this).find('.table-item-add').addClass('d-none');
    });

    $('body').on('mouseenter', '.group-tr', function(){
        $(this).find('.row-close-x').html('&times;');
    }).on('mouseleave', '.group-tr', function(){
        $(this).find('.row-close-x').html('<i class="fas fa-users"></i>');
    });

    $('body').on('mouseover', '.row-close-x', function(){
        $(this).closest('.user-tr').addClass('x-active');
    }).on('mouseout', '.row-close-x', function(){
        $(this).closest('.user-tr').removeClass('x-active');
    });

    $('body').on('click', '.user-tr .row-close-x', function(){
        userid = $(this).closest('tr').attr('data-id');
        $('#removeUser').text($(this).closest('tr').attr('data-user'));
        removeUserButtonId = $(this).closest('tr').attr('id');

        $('#removeUserModal').modal('show');
    });

    $('#removeUserConfirm').click(function(){
        var json = {orgid:localStorage.orgid, userid:userid}

        $.ajax({
            type: 'POST',
            url: "http://127.0.0.1:8085/deleteUser",
            contentType: 'application/json; charset=utf-8',
            dataType: 'JSON',
            data: JSON.stringify(json),
            success: function(response){
                $('#' + removeUserButtonId).closest('tr').remove();
            },
            error: function (xhr, textStatus, errorThrown){
                console.log('error' + errorThrown);
                console.log('xhr', xhr);
                console.log(xhr.responseText);
            }
        });

        $('#removeUserModal').modal('hide');
    });

    $('#addUser').click(function(){
        $('#addUserModal').modal('show');
    });

    $('#newUserForm').on('submit', function(e){
        e.preventDefault();
        var email = $('#userEmail').val();
        var json = {orgid:localStorage.orgid, email:email}

        $.ajax({
            type: 'POST',
            url: "http://127.0.0.1:8085/createUser",
            contentType: 'application/json; charset=utf-8',
            dataType: 'JSON',
            data: JSON.stringify(json),
            success: function(response){
                insertUser(email, response.id);
                $('#userEmail').val('');
                $('#addUserModal').modal('hide');
            },
            error: function (xhr, textStatus, errorThrown){
                $('#addUserModal').modal('hide');
                $('#userEmail').val('');
                if(xhr.responseText == 'duplicate')
                    alert('User already exists');
                console.log(xhr.responseText);
            }
        });
    })

    $('body').on('click', '.group-tr .row-close-x', function(){
        groupid = $(this).closest('tr').attr('data-id');
        $('#removeGroup').text($(this).closest('tr').attr('data-group'));
        removeGroupButtonId = $(this).closest('tr').attr('id');

        $('#removeGroupModal').modal('show');
    });

    $('#addGroup').click(function(){
        $('#addGroupModal').modal('show');
    })

    $('#newGroupForm').on('submit', function(e){
        e.preventDefault();
        var group = $('#groupName').val();

        var json = {orgid:localStorage.orgid, groupname:group}

        $.ajax({
            type: 'POST',
            url: "http://127.0.0.1:8085/createGroup",
            contentType: 'application/json; charset=utf-8',
            dataType: 'JSON',
            data: JSON.stringify(json),
            success: function(response){
                insertGroup(group, response.id);
                $('#groupName').val('');
                $('#addGroupModal').modal('hide');
            },
            error: function (xhr, textStatus, errorThrown){
                $('#addGroupModal').modal('hide');
                $('#groupName').val('');
                if(xhr.responseText == 'duplicate')
                    alert('This group already exists');
                console.log(xhr.responseText);
            }
        });
    })

    $('#removeGroupConfirm').click(function(){
        var json = {orgid:localStorage.orgid, groupid:groupid}

        $.ajax({
            type: 'POST',
            url: "http://127.0.0.1:8085/deleteGroup",
            contentType: 'application/json; charset=utf-8',
            dataType: 'JSON',
            data: JSON.stringify(json),
            success: function(response){
                $('#' + removeGroupButtonId).closest('tr').remove();
            },
            error: function (xhr, textStatus, errorThrown){
                console.log('error' + errorThrown);
                console.log('xhr', xhr);
                console.log(xhr.responseText);
            }
        });

        $('#removeGroupModal').modal('hide');
    });

    $('body').on('click', '.group-add', function(){
        $('.groupOption').remove();
        userid = $(this).closest('tr').attr('data-id');
        trid = $(this).closest('tr').attr('id');

        $.ajax({
            type: 'GET',
            url: "http://127.0.0.1:8085/getGroupNames?orgid=" + localStorage.orgid,
            contentType: 'application/json; charset=utf-8',
            dataType: 'JSON',
            success: function(response){
                response.forEach(function(item, i){
                    var option = document.createElement('option');
                    option.className = 'groupOption';
                    option.setAttribute('value', item.id);
                    option.appendChild(document.createTextNode(item.groupname));
                    document.getElementById('addToGroup').appendChild(option);
                })

                $('#addUserToGroupModal').modal('show');
            },
            error: function (xhr, textStatus, errorThrown){
                console.log('error' + errorThrown);
                console.log('xhr', xhr);
                console.log(xhr.responseText);
            }
        });
    });

    $('#addUserToGroupForm').on('submit', function(e){
        e.preventDefault();

        var json = {orgid:localStorage.orgid, userid:userid , groupid:$('#addToGroup').val()}

        $.ajax({
            type: 'POST',
            url: "http://127.0.0.1:8085/addUserToGroup",
            contentType: 'application/json; charset=utf-8',
            dataType: 'JSON',
            data: JSON.stringify(json),
            success: function(response){
                addGroupToUser($("#addToGroup option:selected").text(), $('#addToGroup').val(), trid);
                $('#addUserToGroupModal').modal('hide');
            },
            error: function (xhr, textStatus, errorThrown){
                $('#addUserToGroupModal').modal('hide');
                if(xhr.responseText == 'duplicate')
                    alert('User is already in selected group');
                console.log(xhr.responseText);
            }
        });
    });
});

var groupid;
var groupname;
var userid;
var trid;
var username;
var removeGroupButtonId;
var removeUserButtonId;

function animateChangeDiv(div1){
    div1.animate({opacity: '1'}, 500, function(){
    });
}

function getUsers(){
    $.ajax({
        type: 'GET',
        url: "http://127.0.0.1:8085/getUsersAndGroups?orgid=" + localStorage.orgid,
        contentType: 'application/json; charset=utf-8',
        dataType: 'JSON',
        success: function(response){
            buildUserList(response);
        },
        error: function (xhr, textStatus, errorThrown){
            console.log('error' + errorThrown);
            console.log('xhr', xhr);
            console.log(xhr.responseText);
        }
    });
}

function getGroups(){
    $.ajax({
        type: 'GET',
        url: "http://127.0.0.1:8085/getGroups?orgid" + localStorage.orgid,
        contentType: 'application/json; charset=utf-8',
        dataType: 'JSON',
        success: function(response){
            buildGroupList(response);
        },
        error: function (xhr, textStatus, errorThrown){
            console.log('error' + errorThrown);
            console.log('xhr', xhr);
            console.log(xhr.responseText);
        }
    });
}

function insertGroup(group, id){
    var tr = buildGroup({groupname:group, id:id, users:0});
    var children = $('#groupBody').children();
    for(var x = 0; x < children.length; x++){
        if(group.toLowerCase() < $(children[x]).attr('data-group').toLowerCase()){
            $(tr).insertBefore($(children[x]));
            return;
        }
    }
    document.getElementById('groupBody').appendChild(tr);
}

function buildGroupList(groups){
    for(var x = 0; x < groups.length; x++){
        document.getElementById('groupBody').appendChild(buildGroup(groups[x]));
    }
}

function buildGroup(group){
    var tr = document.createElement('tr');
    tr.setAttribute('data-id', group.id);
    tr.setAttribute('data-group', group.groupname);
    tr.className = 'group-tr';
    tr.id = guid();
    var td0 = document.createElement('td');
    td0.className = 'py-0';
    var xspan = document.createElement('span');
    xspan.className = 'row-close-x ';
    $(xspan).html('<i class="fas fa-users"></i>');
    var td1 = document.createElement('td');
    var name = document.createElement('p');
    name.appendChild(document.createTextNode(group.groupname));
    var td2 = document.createElement('td');
    var count = document.createElement('p');
    count.appendChild(document.createTextNode(group.users));

    tr.appendChild(td0);
    td0.appendChild(xspan);
    tr.appendChild(td1);
    td1.appendChild(name);
    tr.appendChild(td2);
    td2.appendChild(count);

    return tr;
}

function addGroupToUser(name, id, trID){
    var item = document.createElement('span');
    item.className='table-item-remove';
    var group = document.createElement('span');
    group.appendChild(document.createTextNode(name));
    var close = document.createElement('span');
    close.className = 'item-close-x d-none ml-1';
    close.id = guid();
    close.setAttribute('data-id', id);
    $(close).html('&times;');
    item.appendChild(group);
    item.appendChild(close);

    $('#' + trID).find('.group-list').prepend(item);
}

function insertUser(email, id){
    var tr = buildUser({email:email, id:id});
    document.getElementById('userBody').insertBefore(tr, document.getElementById('userBody').firstChild);
}

function buildUserList(users){
    for(var x = 0; x < users.length; x++){
        document.getElementById('userBody').appendChild(buildUser(users[x]));
    }
}

function buildUser(user){
    var tr = document.createElement('tr');
    tr.setAttribute('data-id', user.id);
    if(user.username != null)
        tr.setAttribute('data-user', user.username);
    else
        tr.setAttribute('data-user', user.email);
    tr.className = 'user-tr';
    tr.id = guid();
    var td0 = document.createElement('td');
    var xspan = document.createElement('span');
    xspan.className = 'my-auto ml-3 row-close-x';
    $(xspan).html('<i class="fas fa-user"></i>');

    var td1 = document.createElement('td');
    var tdrow1 = document.createElement('div');
    tdrow1.className = 'row';

    var col = document.createElement('div');
    col.className = 'col';
    var row1 = document.createElement('div');
    row1.className = 'row ml-0';
    var name = document.createElement('p');
    if(user.username != null)
        name.appendChild(document.createTextNode(user.username));
    else
        name.appendChild(document.createTextNode('Waiting for invite'));
    var row2 = document.createElement('div');
    row2.className = 'row ml-0';
    var email = document.createElement('small');
    email.appendChild(document.createTextNode(user.email));
    var td2 = document.createElement('td');
    var groupRow = document.createElement('div');
    groupRow.className = 'row group-list';

    if(user.groupnames != null){
        user.groupnames.split(',').forEach(function(gp, i){
            var groupinfo = gp.split('~|$');

            var item = document.createElement('span');
            item.className='table-item-remove';
            var group = document.createElement('span');
            group.appendChild(document.createTextNode(groupinfo[0]));
            var close = document.createElement('span');
            close.className = 'item-close-x d-none ml-1';
            close.id = guid();
            close.setAttribute('data-id', groupinfo[1]);
            $(close).html('&times;');
            item.appendChild(group);
            item.appendChild(close);
            groupRow.appendChild(item);
        });
    }

    var addspan = document.createElement('span');
    addspan.className = 'table-item-add group-add d-none';
    var plusSpan = document.createElement('span');
    plusSpan.appendChild(document.createTextNode('+'));

    addspan.append(plusSpan);
    groupRow.appendChild(addspan);
    
    var td3 = document.createElement('td');

    tr.appendChild(td0);
    td0.appendChild(xspan);
    tr.appendChild(td1);
    td1.appendChild(tdrow1);
    tdrow1.appendChild(col);
    col.appendChild(row1);
    row1.appendChild(name);
    col.appendChild(row2);
    row2.appendChild(email);
    tr.appendChild(td2);
    td2.appendChild(groupRow);
    tr.appendChild(td3);

    return tr;
}

function guid() {
    function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}