$(function(){
    
    $('body').on('click', '.list-group-item', function(){
        $('#myTasklist').css('opacity', 0);
        $('#myTasklist').addClass('d-none');
        $('#stepContainer').removeClass('d-none');
        $('#stepContainer').css('opacity', 0);
        $('#backButton').removeClass('d-none');
        $('#stepContainer').empty();
        animateChangeDiv($('#stepContainer'));

        var id = $(this).attr('data-id');
        listId = id;
        $('#navText').text($(this).attr('data-title'));
        $.ajax({
            type: 'GET',
            url: "http://127.0.0.1:8085/getAssignedTasks?taskListId=" + id + "&user=" + localStorage.userId,
            contentType: 'application/json; charset=utf-8',
            dataType: 'JSON',
            success: function(response){
                buildTaskList(response);
            },
            error: function (xhr, textStatus, errorThrown){
                console.log('error' + errorThrown);
                console.log('xhr', xhr);
                console.log(xhr.responseText);
            }
        });
    });

    $('body').on('change', '.completeCheck', function(){
        if($(this).is(':checked')){
            $(this).closest('div').find('i').removeClass('fa-square').addClass('fa-check-square');
            $(this).closest('label').addClass('complete-button').removeClass('btn-outline-light');

            saveTask($(this).closest('div').attr('data-id'));
        }
        else{
            $(this).closest('div').find('i').removeClass('fa-check-square').addClass('fa-square');
            $(this).closest('label').addClass('btn btn-outline-light').removeClass('complete-button');

            uncomplete($(this).closest('div').attr('data-id'));
        }
    });

    $('#backButton').on('click', function(){
        $('#stepContainer').css('opacity', 0);
        $('#stepContainer').addClass('d-none');
        $('#myTasklist').removeClass('d-none');
        $('#taskLists').empty();
        $('#backButton').addClass('d-none');
        $('#navText').text('My Task Lists');
        fields = {};
        completed = {};

        $.ajax({
            type: 'GET',
            url: "http://127.0.0.1:8085/getRunningTasks?orgid=" + localStorage.orgid + "&user=" + localStorage.userId,
            contentType: 'application/json; charset=utf-8',
            dataType: 'JSON',
            success: function(response){
                buildList(response);
                animateChangeDiv($('#myTasklist'));
            },
            error: function (xhr, textStatus, errorThrown){
                console.log('error' + errorThrown);
                console.log('xhr', xhr);
                console.log(xhr.responseText);
            }
        });
    });

    $('#completeList').click(function(){
        var task = {taskListId:listId, userId:localStorage.userId}
        $.ajax({
            type: 'POST',
            url: "http://127.0.0.1:8085/completeList",
            contentType: 'application/json; charset=utf-8',
            dataType: 'JSON',
            data: JSON.stringify(task),
            success: function(response){
                console.log('list completed for', listId);
                $('#backButton').trigger('click');
                $('#completeModal').modal('hide');
            },
            error: function (xhr, textStatus, errorThrown){
                console.log('error' + errorThrown);
                console.log('xhr', xhr);
                console.log(xhr.responseText);
            }
        });
    });
});

function saveTask(id){
    for(var key in fields[id]){
        if(fields[id][key] == 'textbox')
            fields[id][key] = $('#' + key).val();
        else if(fields[id][key] == 'textarea')
            fields[id][key] = $('#' + key).val();

            $('#' + key).attr('disabled', true);
    }

    var task = {id:id, values:fields[id]}
    $.ajax({
        type: 'POST',
        url: "http://127.0.0.1:8085/completeTask",
        contentType: 'application/json; charset=utf-8',
        dataType: 'JSON',
        data: JSON.stringify(task),
        success: function(response){
            console.log('task updated');
            completed[id] = true;
            checkTasksComplete();
        },
        error: function (xhr, textStatus, errorThrown){
            console.log('error' + errorThrown);
            console.log('xhr', xhr);
            console.log(xhr.responseText);
        }
    });
}

function checkTasksComplete(){
    for(var key in completed){
        if(!completed[key])
            return;
    }
    
    $('#completeModal').modal('show');
}

function uncomplete(id){
    for(var key in fields[id]){
        $('#' + key).attr('disabled', false);
    }

    var task = {id:id}
    $.ajax({
        type: 'POST',
        url: "http://127.0.0.1:8085/uncompleteTask",
        contentType: 'application/json; charset=utf-8',
        dataType: 'JSON',
        data: JSON.stringify(task),
        success: function(response){
            console.log('task updated');
            completed[id] = false;
        },
        error: function (xhr, textStatus, errorThrown){
            console.log('error' + errorThrown);
            console.log('xhr', xhr);
            console.log(xhr.responseText);
        }
    });
}

var fields = {};
var completed = {}
var listId;

function buildTaskList(tasks){
    for(var x = 0; x < tasks.length; x++){
        var json = JSON.parse(tasks[x].taskSchema);
        fields[tasks[x].id] = {};
        completed[tasks[x].id] = false;
        var div = document.createElement('div');
        div.className = 'row mt-3';
        div.id = tasks[x].id;
        var col1 = document.createElement('div');
        col1.className = 'col-1 taskHeader text-center';
        var row = document.createElement('div');
        row.className = 'row h-100';
        var span = document.createElement('span');
        span.className = 'my-auto mx-auto taskNumber';

        var col2 = document.createElement('div');
        col2.className = 'col-11 taskBody';
        var form = document.createElement('form');
        form.className = 'mt-2';

        var titleRow = document.createElement('div');
        titleRow.className = 'row';
        var titleCol = document.createElement('div');
        titleCol.className = 'col';

        var fg1 = document.createElement('div');
        fg1.className = 'form-group';

        var buttonCol = document.createElement('div');
        buttonCol.className = 'col-auto';
        buttonCol.id = 'col' + tasks[x].id;

        var toggleButton = '';
        var complete = tasks[x].complete;
        if(complete){
            toggleButton = '<div class="btn-group-toggle"  data-id="' + tasks[x].id + '">' +
                                '<label class="btn complete-button">' +
                                    '<input class="completeCheck" type="checkbox" autocomplete="off" checked><i class="far fa-check-square checkIcon"></i> Complete' +
                                '</label>' +
                            '</div>';
            completed[tasks[x].id] = true;
        }
        else
            toggleButton = '<div class="btn-group-toggle"  data-id="' + tasks[x].id + '">' +
                                '<label class="btn btn-outline-light">' +
                                    '<input class="completeCheck" type="checkbox" autocomplete="off"><i class="far fa-square checkIcon"></i> Complete' +
                                '</label>' +
                            '</div>';
        
        var title = document.createElement('p');
        title.className = 'title';
        var fg2 = document.createElement('div');
        fg2.className = 'form-group mb-0';
        var inst = document.createElement('p');
        inst.className = 'instructions mb-5'

        span.appendChild(document.createTextNode(json.index));
        title.appendChild(document.createTextNode(json.title));
        inst.appendChild(document.createTextNode(json.instructions));

        div.appendChild(col1);
        col1.appendChild(row);
        row.appendChild(span);
        div.appendChild(col2);
        col2.appendChild(form);
        form.appendChild(fg1);
        form.appendChild(titleRow);
        titleRow.appendChild(titleCol);
        titleCol.appendChild(title);
        titleRow.appendChild(buttonCol);

        form.appendChild(fg2);
        fg2.appendChild(inst);

        for(var i = 0; i < json.elements.length; i++){
            var element = json.elements[i];
            if(element.type == 'textbox'){
                form.appendChild(createTextBox(element, tasks[x].id));
            }
            else if(element.type == 'textarea'){
                form.appendChild(createTextArea(element, tasks[x].id));
            }
        }

        document.getElementById('stepContainer').appendChild(div);
        $('#col' + tasks[x].id).append(toggleButton);

        var task = tasks[x];
        const taskValues = JSON.parse(tasks[x].taskValues);
        if(taskValues == null)
            continue;
            
        for(var key in taskValues){
            var tagname = $('#' + key)[0].tagName;
            if(tagname === 'INPUT')
                $('#' + key).val(taskValues[key]);
            else if(tagname == 'TEXTAREA')
                $('#' + key).val(taskValues[key]);

            if(complete)
                $('#' + key).attr('disabled', true);
        }
    }
    checkTasksComplete();
}

function createTextBox(element, taskId){
    fields[taskId][element.id] = 'textbox';
    var div = document.createElement('div');
    div.className = 'form-group';

    var label = document.createElement('label');
    label.appendChild(document.createTextNode(element.label));
    label.setAttribute('for', element.id);

    var input = document.createElement('input');
    input.id = element.id;
    input.type = 'text';
    input.className = 'form-control';
    input.setAttribute('aria-describedby', 'small' + element.id);
    if(element.inputType == 'placeholder')
        input.setAttribute('placeholder', element.input);
    else
        input.value = element.input;
    if(element.readonly)
        input.setAttribute('readonly', true);

    var small = document.createElement('small');
    small.className = 'form-text';
    small.appendChild(document.createTextNode(element.small));
    small.id = 'small' + element.id;

    div.appendChild(label);
    div.appendChild(input);
    div.appendChild(small);

    return div;
}

function createTextArea(element, taskId){
    fields[taskId][element.id] = 'textarea';
    var div = document.createElement('div');
    div.className = 'form-group';

    var label = document.createElement('label');
    label.appendChild(document.createTextNode(element.label));
    label.setAttribute('for', element.id);

    var textarea = document.createElement('textarea');
    textarea.className = 'form-control';
    textarea.id = element.id;
    textarea.setAttribute('rows', element.rows);

    div.appendChild(label);
    div.appendChild(textarea);

    return div;
}

function animateChangeDiv(div1){
    div1.animate({opacity: '1'}, 500, function(){
    });
}

function buildList(tasks){
    for(var x = 0; x < tasks.length; x++){
        var div = document.createElement('div');
        div.className = "list-group-item list-group-item-action";
        div.setAttribute('data-id', tasks[x].id);
        div.setAttribute('data-title', tasks[x].taskListTitle);
        var row = document.createElement('div');
        row.className = 'row';
        var col = document.createElement('div');
        col.className = 'col-auto';
        var h1 = document.createElement('h1');
        var i = document.createElement('i');
        i.className = 'fas fa-clipboard-list';
        var col2 = document.createElement('div');
        col2.className = 'col';
        var div1 = document.createElement('div');
        div1.className = 'd-flex w-100 justify-content-between';
        var h5 = document.createElement('h5');
        h5.className = 'mb-1';
        var small = document.createElement('small');
        var div2 = document.createElement('div');
        div2.className = 'd-flex w-100 justify-content-between';
        var p = document.createElement('p');
        var p2= document.createElement('p');

        h5.appendChild(document.createTextNode(tasks[x].taskListTitle));
        small.appendChild(document.createTextNode(getDuration(tasks[x].runningTime)))
        p.appendChild(document.createTextNode('Created by ' + tasks[x].username));
        p2.appendChild(document.createTextNode(tasks[x].completed + ' of ' + tasks[x].total + ' Tasks Completed'));

        div.appendChild(row);
        row.appendChild(col);
        col.appendChild(h1);
        h1.appendChild(i);
        row.appendChild(col2);
        col2.appendChild(div1);
        div1.appendChild(h5);
        div1.appendChild(small);
        col2.appendChild(div2);
        div2.appendChild(p);
        div2.appendChild(p2);

        document.getElementById('taskLists').appendChild(div);
    }
}

function getDuration(time) {
    let comp = time.split(":");
    
    var days = Math.floor(comp[0] / 24);
    if(days >= 1){
        if(days > 1)
            return days + ' days ago';
        return '1 day ago';
    }
    if(comp[0] > 0){
        if(comp[0] > 1) 
            return parseInt(comp[0], 10) + ' hours ago';
        return '1 hour ago';
    }
    if(comp[1] > 0){
        if(comp[1] > 1)
            return parseInt(comp[1], 10) + ' minutes ago';
        return '1 minute ago';
    }
    return 'Just now';
  }