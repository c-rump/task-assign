

$(function(){
    $('body').on('click', '.form-builder-options li ul li', function(){
        var item = $(this).text();
        if(item == 'Text Box'){
            createTextBox($(this).closest('div').attr('data-id'));
        }
        else if(item == 'Text Area'){
            createTextArea($(this).closest('div').attr('data-id'));
        }
    });

    $('#taskListName').tooltip({'trigger':'focus', 'placement':'right', 'title': 'The name you will see'});
    $('#taskListTitle').tooltip({'trigger':'focus', 'placement':'right', 'title': 'The name users will see'})

    $('textarea').focusout(function(){
        var lht = parseInt($(this).css('lineHeight'),10);
        var lines = this.scrollHeight / lht;
        if(lines >= 4)
            $(this).height(0).height(this.scrollHeight);
    });

    $(document).on('click', '.dropdown-item.checkedDropdown', function(event) {
        event.preventDefault();
        $(this).toggleClass('dropdown-item-checked').siblings().removeClass('dropdown-item-checked');  
    });   

    $('body').on('click', '.textBoxDropdown a', function(){
        var id = $(this).attr('data-for');
		$('#' + id).prop('readonly', false);
		if(this.text == 'Text'){
            $('#' + id).attr('type', 'text');
            $('#' + id).removeClass('text-muted');
        }
        else if(this.text == 'Placeholder'){
            $('#' + id).addClass('text-muted');
        }
		else if(this.text == 'Read Only'){
			$('#' + id).prop('readonly', true);
			$('#' + id).attr('type', 'text');
        }
        else if(this.text == 'Delete'){
            $('#div' + id).remove();
        }
    });

    $('body').on('click', '.textareaSettings a', function(){
        var id = $(this).attr('data-for');
        if(this.text == 'Delete'){
            $('#div' + id).remove();
        }
        else{
            $('#' + id).attr('rows', this.text);
        }
    });

    $('#plusButton').on('click', function(){
        addNewStep(++steps);
    });

    $('body').on('click', '.step-close', function(){
        animateDelete($('#' + $(this).attr('data-id')));
    });

    $('body').on('click', '.step-up', function(){
        var div = $('#' + $(this).attr('data-id'));
        if(!div.is(":first-child")){
            var prev = div.prev();
            div.insertBefore(prev);
            animateRenumber(div.parent().children().first(), 1);
        }
    });

    $('body').on('click', '.step-down', function(){
        var div = $('#' + $(this).attr('data-id'));
        if(!div.is(":last-child")){
            var next = div.next();
            div.insertAfter(next);
            animateRenumber(div.parent().children().first(), 1);
        }
    });

    $('body').on('click', '.btn-circle-inactive', function(){
        $('.btn-circle-active').removeClass('btn-circle-active').addClass('btn-circle-inactive');
        $(this).removeClass('btn-circle-inactive').addClass('btn-circle-active');
        $('.content-container').addClass('d-none');
        $('.content-container').removeClass('d-flex');
        
        var selected = $(this).next().text();
        if(selected == 'Properties'){
            $('#propContainer').css('opacity', 0);
            $('#propContainer').removeClass('d-none').addClass('d-flex');
            animateChangeDiv($('#propContainer'));
        }
        else if(selected == 'Delivery Defaults'){
            $('#deliveryContainer').css('opacity', 0);
            $('#deliveryContainer').removeClass('d-none');
            animateChangeDiv($('#deliveryContainer'));
        }
        else if(selected == 'Task List'){
            $('#stepContainerWrapper').css('opacity', 0);
            $('#stepContainerWrapper').removeClass('d-none');
            animateChangeDiv($('#stepContainerWrapper'));
        }
        else if(selected == 'Save'){
            var error = false;
            if($('#taskListName').val().trim() == ''){
                $('#noNameAlert').removeClass('d-none');
                $('#save').attr('disabled', true);
                error = true;
            }
            else{
                $('#noNameAlert').addClass('d-none');
            }
            if($('#taskListTitle').val().trim() == ''){
                $('#noTitleAlert').removeClass('d-none');
                $('#save').attr('disabled', true);
                error = true;
            }
            else{
                $('#noTitleAlert').addClass('d-none');
            }
            if($('#stepContainer').children().length == 0){
                $('#noTaskAlert').removeClass('d-none');
                $('#save').attr('disabled', true);
                error = true;
            }
            else{
                $('#noTaskAlert').addClass('d-none');
            }

            if(!error){
                $('#save').attr('disabled', false);
            }

            $('#saveContainer').css('opacity', 0);
            $('#saveContainer').removeClass('d-none');
            animateChangeDiv($('#saveContainer'));
        }
    });

    $('#recipientList').on('click', '.removeButton', function(e){
        e.preventDefault();
        $(this).parent().remove();
    });

    $(document).ready(function() {
        $(window).keydown(function(event){
            if(event.target.id == 'addRecipient' && event.keyCode == 13){
                event.preventDefault();

                var recipient = $('#addRecipient').val();

                if(recipient == '' || typeof reciepient != typeof undefined)
                    return false;
                
                if(validateUser(recipient))
                    addRecipientElement();
                
                return false;
            }
            else if(event.keyCode == 13) {
                //event.preventDefault();
                //return false;
            }
        });
    });

    function validateUser(recipient){
        recipient = recipient.toLowerCase();
        if(!(recipient in users)){
            return false;
        }
        if(!recipientAlreadyAdded(recipient))
            return true;
        return false;
    }

    $('#addRecipientButton').on('click', function(){
        var recipient = $('#addRecipient').val();
        if(recipient == '' || typeof reciepient != typeof undefined)
            return;
        if(validateUser(recipient))
                addRecipientElement();
    });

    function addRecipientElement(){
        var li = document.createElement('li');
        li.className = 'list-group-item d-flex justify-content-between align-items-center list-group-item-action';
        li.appendChild(document.createTextNode($('#addRecipient').val()));
        var span = document.createElement('span');
        span.className = 'fas fa-times removeButton';
        li.appendChild(span);
        document.getElementById('recipientList').appendChild(li);
        $('#addRecipient').val('');
    }

    function recipientAlreadyAdded(recipient){
        var found = false;
        $('#recipientList').children().each(function(){
            if($(this).text().toLowerCase() == recipient){
                found = true;
            }
        });
        if(found)
            return true;
        return false;
    }

    $('#deliveryMethod').on('change', function(){
        if(this.value == 'group'){
            $('.individualItems').addClass('d-none');
            $('#groupName').removeClass('d-none');
            $('#whatToDeliver').removeClass('d-none');
        }
        else if(this.value == 'individual'){
            $('.groupItems').addClass('d-none');
            $('#recipients').removeClass('d-none');
            $('#deliveryMethodChoices').removeClass('d-none');
        }
    });

    $('#save').click(function(){
        var json = getJson();
        var task = {taskListName:json.name, orgid:localStorage.orgid, json:json}
        $.ajax({
            type: 'POST',
            url: "http://127.0.0.1:8085/saveTask",
            contentType: 'application/json; charset=utf-8',
            dataType: 'JSON',
            data: JSON.stringify(task),
            success: function(response){
                window.location = 'createTask.html';
            },
            error: function (xhr, textStatus, errorThrown){
                console.log('error' + errorThrown);
                console.log('xhr', xhr);
                console.log(xhr.responseText);
            }
        });
    });

    
});

var steps = 1;

function renumber(){
    var num = 1;
    $('#stepContainer > div').each(function(){
        $(this).find('.taskNumber').text(num);
        num++;
    })
}

function animateRenumber(div, num){
    if(div == null)
        return;
    var number = div.find('.taskNumber');
    number.animate({fontSize: '80px'}, 100, function(){
        number.animate({fontSize: '60px'}, 100, function(){
            number.text(num);
            animateRenumber(div.next(), num + 1);
        });
    });
}

function animateDelete(div){
    div.animate({opacity: '0'}, 150, function(){
        div.animate({height: '0px'}, 150, function(){
            div.remove();
            steps--;
            animateRenumber($('#stepContainer').children().first(), 1);
        });
    });
}

function animateChangeDiv(div1){
    div1.animate({opacity: '1'}, 500, function(){
    });
}

function getJson(){
    var json = {
        name:$('#taskListName').val(),
        title:$('#taskListTitle').val(),
        tasks:[],
        delivery : {
            users:[]
        }
    };
    
    $('#stepContainer').children().each(function(i, div){
        var id = div.id;
        var task = {
            id:id,
            index:i + 1,
            title : $('#title' + id).val(),
            instructions : $('#instructions' + id).val(),
            elements : []
        }
        $('#contentBody' + id).children().each(function(i, element){
            if(this.getAttribute('data-element') == 'textbox'){
                var divId = this.getAttribute('data-id');
                var label = $('#label' + divId).val();
                var small = $('#small' + divId).val();
                var input = $('#' + divId).val();
                var type;
                var readonly;
                if($('#' + divId).hasClass('text-muted'))
                    type = 'placeholder';
                else
                    type = 'text';
                var attr = $('#' + divId).attr('readonly');
                if (typeof attr !== typeof undefined && attr !== false) {
                    readonly = true;
                }
                else 
                    readonly = false;
                task.elements.push({
                    id:divId,
                    type:'textbox',
                    label:label,
                    small:small,
                    input:input,
                    inputType:type,
                    readonly:readonly
                });
            }
            else if(this.getAttribute('data-element') == 'textarea'){
                var divId = this.getAttribute('data-id');
                task.elements.push({
                    id:divId,
                    type:'textarea',
                    label:$('#label' + divId).val(),
                    rows:$('#' + divId).attr('rows')
                });
            }
        });
        json.tasks.push(task);
    });

    if($('#deliveryMethod').val() == 'individual'){
        json.delivery['type'] = 'individual';
        $('#recipientList').children().each(function(){
            json.delivery.users.push($(this).text());
        });

        if(typeof $("input:radio[name='howChoices']:checked").val() !== typeof undefined && $("input:radio[name='howChoices']:checked").val() != ''){
            json.delivery['choice'] = $("input:radio[name='howChoices']:checked").val();
        }
    }
    else if($('#deliveryMethod').val() == 'group'){
        json.delivery['type'] = 'group';

        json.delivery['groupname'] = $('#groups').val();

        if(typeof $("input:radio[name='whatChoices']:checked").val() !== typeof undefined && $("input:radio[name='whatChoices']:checked").val() != ''){
            json.delivery['choice'] = $("input:radio[name='whatChoices']:checked").val();
        }
    }

    return json;
}

function addNewStep(number){
    var id = guid();
    var div = document.createElement('div');
    div.className = 'row mt-3';
    div.id = id;
    var col1 = document.createElement('div');
    col1.className = 'col-1 taskHeader text-center';
    var row = document.createElement('div');
    row.className = 'row h-100';
    var taskNumber = document.createElement('span');
    taskNumber.className = 'my-auto mx-auto taskNumber';
    
    div.appendChild(col1);
    col1.appendChild(row);
    row.appendChild(taskNumber)
    taskNumber.appendChild(document.createTextNode(number));

    var body = document.createElement('div');
    body.className = 'col-11 taskBody';
    var form = document.createElement('form');
    form.className = 'mt-2';
    var titleGroup = document.createElement('div');
    titleGroup.className = 'form-group';
    var title = document.createElement('input');
    title.type = 'text';
    title.className = 'borderless title';
    title.placeholder = 'Title';
    title.setAttribute('required', true);
    title.id = 'title' + id;
    var instGroup = document.createElement('div');
    instGroup.className = 'form-group mb-0';
    var inst = document.createElement('textarea');
    inst.className = 'borderless';
    inst.rows = 3;
    inst.placeholder = 'Instructions';
    inst.id = 'instructions' + id;
    var content = document.createElement('div');
    content.id = 'contentBody' + id;

    div.appendChild(body);
    body.appendChild(form);
    form.appendChild(titleGroup);
    titleGroup.appendChild(title);
    form.appendChild(instGroup);
    instGroup.appendChild(inst);
    form.appendChild(content);

    var formGroup = document.createElement('div');
    formGroup.className = 'form-row mb-0 taskControls justify-content-between';
    var add = document.createElement('button');
    add.className = 'btn btn-primary-outline dropdown-toggle taskControlDropdown py-0 step-control';
    add.type = 'button';
    add.id = 'task' + id + 'dropdown';
    add.setAttribute('data-toggle', 'dropdown');
    add.setAttribute('aria-haspopup', true);
    add.setAttribute('aria-expanded', false);
    var span = document.createElement('span');
    var i = document.createElement('i');
    i.className = 'far fa-plus-square';
    var dropdown = document.createElement('div');
    dropdown.className = 'dropdown-menu';
    dropdown.setAttribute('aria-labelledby', 'task' + id + 'dropdown');
    var arrowDiv = document.createElement('div');
    var up = document.createElement('span');
    up.className = 'step-control step-up';
    up.setAttribute('data-id', id);
    var upI = document.createElement('i');
    upI.className = 'fas fa-chevron-up mr-2';
    var down = document.createElement('span');
    down.className = 'step-control step-down';
    down.setAttribute('data-id', id);
    var downI = document.createElement('i');
    downI.className = 'fas fa-chevron-down';
    var close = document.createElement('span');
    close.className = 'step-control step-close';
    close.setAttribute('data-id', id);
    var closeI = document.createElement('i');
    closeI.className = 'far fa-window-close mr-2';

    form.appendChild(formGroup);
    formGroup.appendChild(add);
    add.appendChild(span);
    span.appendChild(i);
    formGroup.appendChild(dropdown);
    formGroup.appendChild(arrowDiv);
    arrowDiv.appendChild(up);
    arrowDiv.appendChild(down);
    up.appendChild(upI);
    down.appendChild(downI);
    formGroup.appendChild(close);
    close.appendChild(closeI);

    var options = document.createElement('div');
    options.setAttribute('data-id', id);
    options.className = 'form-builder-options row';
    var li1 = document.createElement('li');
    li1.className = 'col-sm-6';
    var ul1 = document.createElement('ul');
    var liHeader = document.createElement('li');
    liHeader.className = 'dropdown-header';
    liHeader.appendChild(document.createTextNode('Form'));
    var divider1 = document.createElement('div');
    divider1.className = 'dropdown-divider';
    var litb = document.createElement('li');
    var spantb = document.createElement('span');
    var lita = document.createElement('li');
    var spanta = document.createElement('span');
    var lidd = document.createElement('li');
    var spandd = document.createElement('span');
    var lirb = document.createElement('li');
    var spanrb = document.createElement('span');
    var licb = document.createElement('li');
    var spancb = document.createElement('span');
    var li2 = document.createElement('li');
    li2.className = 'col-sm-6';
    var ul2 = document.createElement('ul');
    var liHeader2 = document.createElement('li');
    liHeader2.className = 'dropdown-header';
    liHeader2.appendChild(document.createTextNode('Text'));
    var divider2 = document.createElement('div');
    divider2.className = 'dropdown-divider';
    var lili = document.createElement('li');
    var spanli = document.createElement('span');
    var lip = document.createElement('li');
    var spanp = document.createElement('span');

    dropdown.appendChild(options);
    options.appendChild(li1);
    li1.appendChild(ul1);
    ul1.appendChild(liHeader);
    ul1.appendChild(divider1);
    ul1.appendChild(litb);
    litb.appendChild(spantb);
    spantb.appendChild(document.createTextNode('Text Box'));
    ul1.appendChild(lita);
    lita.appendChild(spanta);
    spanta.appendChild(document.createTextNode('Text Area'));
    ul1.appendChild(lidd);
    lidd.appendChild(spandd);
    spandd.appendChild(document.createTextNode('Dropdown'));
    ul1.appendChild(lirb);
    lirb.appendChild(spanrb);
    spanrb.appendChild(document.createTextNode('Radio Button'));
    ul1.appendChild(licb);
    licb.appendChild(spancb);
    spancb.appendChild(document.createTextNode('Checkbox'));
    options.appendChild(li2);
    li2.appendChild(ul2);
    ul2.appendChild(liHeader2);
    ul2.appendChild(divider2);
    ul2.appendChild(lili);
    lili.appendChild(spanli);
    spanli.appendChild(document.createTextNode('Link'));
    ul2.appendChild(lip);
    lip.appendChild(spanp);
    spanp.appendChild(document.createTextNode('Paragraph'));

    $('#stepContainer').append(div);
}

function createTextBox(taskId){
    var id = guid();

    var div = document.createElement('div');
    div.className = 'form-group';
    div.setAttribute('data-id', id);
    div.setAttribute('data-element', 'textbox');
    div.id = 'div' + id;
    var label = document.createElement('input');
    label.className = 'borderless';
    label.value = 'Label';
    label.id = 'label' + id;
    var row = document.createElement('div');
    row.className = 'row';
    var col1= document.createElement('div');
    col1.className = 'col pr-0';
    var input = document.createElement('input');
    input.id = id;
    input.type = 'text';
    input.className = 'form-control text-muted';
    input.setAttribute('aria-describedby', 'small' + id);
    input.setAttribute('placeholder', 'Placeholder text');
    var col2 = document.createElement('div');
    col2.className = 'col-auto px-2 py-0';
    var dropdown = document.createElement('div');
    dropdown.className = 'dropdown';
    var settings = document.createElement('button');
    settings.className = 'btn btn-secondary dropdown-toggle';
    settings.type = 'button';
    settings.setAttribute('data-toggle', 'dropdown');
    settings.setAttribute('aria-haspopup', 'true');
    settings.setAttribute('aria-expanded', 'false');
    var cog = document.createElement('i');
    cog.className = 'fas fa-cog';
    var menu = document.createElement('div');
    menu.className = 'dropdown-menu dropdown-menu-right textBoxDropdown';
    menu.setAttribute('aria-labelledby', 'settings' + id);
    var h6 = document.createElement('h6');
    var a1 = document.createElement('a');
    var a2 = document.createElement('a');
    var a3 = document.createElement('a');
    var divider = document.createElement('div');
    var deleteItem = document.createElement('a');
    a1.setAttribute('data-for', id);
    a2.setAttribute('data-for', id);
    a3.setAttribute('data-for', id);
    deleteItem.setAttribute('data-for', id);
    h6.className = 'dropdown-header';
    a1.className = 'dropdown-item dropdown-item-checked checkedDropdown';
    a2.className = 'dropdown-item checkedDropdown';
    a3.className = 'dropdown-item checkedDropdown';
    divider.className = 'dropdown-divider';
    deleteItem.className = 'dropdown-item';
    var small = document.createElement('input');
    small.className = 'borderless helper-text';
    small.value = 'Helper Text';
    small.id = 'small' + id;

    div.appendChild(label);
    div.appendChild(row);
    row.appendChild(col1);
    col1.appendChild(input);
    row.appendChild(col2);
    col2.appendChild(dropdown);
    dropdown.appendChild(settings);
    settings.appendChild(cog);
    dropdown.appendChild(menu);
    menu.appendChild(h6);
    menu.appendChild(a1);
    menu.appendChild(a2);
    menu.appendChild(a3);
    menu.appendChild(divider);
    menu.appendChild(deleteItem);
    h6.appendChild(document.createTextNode('Options'));
    a1.appendChild(document.createTextNode('Placeholder'));
    a2.appendChild(document.createTextNode('Text'));
    a3.appendChild(document.createTextNode('Read Only'));
    deleteItem.appendChild(document.createTextNode('Delete'));
    div.append(small);
    small.append(document.createTextNode('Helper text'));

    document.getElementById('contentBody' + taskId).appendChild(div);
}

function createTextArea(taskId){
    var id = guid();

    var div = document.createElement('div');
    div.className = 'form-group';
    div.setAttribute('data-id', id);
    div.setAttribute('data-element', 'textarea');
    div.id = 'div' + id;
    var label = document.createElement('input');
    label.className = 'borderless';
    label.value = 'Label';
    label.id = 'label' + id;
    var row = document.createElement('div');
    row.className = 'row';
    var col1= document.createElement('div');
    col1.className = 'col pr-0';
    var textarea = document.createElement('textarea');
    textarea.rows = 3;
    textarea.id = id;
    textarea.className = 'form-control';
    var col2 = document.createElement('div');
    col2.className = 'col-auto my-auto p-2';
    var dropdown = document.createElement('div');
    dropdown.className = 'dropdown';
    var settings = document.createElement('button');
    settings.className = 'btn btn-secondary dropdown-toggle';
    settings.type = 'button';
    settings.setAttribute('data-toggle', 'dropdown');
    settings.setAttribute('aria-haspopup', 'true');
    settings.setAttribute('aria-expanded', 'false');
    settings.id = 'settings' + id;
    var cog = document.createElement('i');
    cog.className = 'fas fa-cog';
    var menu = document.createElement('div');
    menu.className = 'dropdown-menu dropdown-menu-right textareaSettings';
    menu.setAttribute('aria-labelledby', 'settings' + id);
    var h6 = document.createElement('h6');
    var a1 = document.createElement('a');
    var a2 = document.createElement('a');
    var a3 = document.createElement('a');
    var a4 = document.createElement('a');
    var a5 = document.createElement('a');
    var a6 = document.createElement('a');
    var a7 = document.createElement('a');
    var a8 = document.createElement('a');
    var divider = document.createElement('div');
    var a9 = document.createElement('a');
    h6.setAttribute('data-for', id);
    a1.setAttribute('data-for', id);
    a2.setAttribute('data-for', id);
    a3.setAttribute('data-for', id);
    a4.setAttribute('data-for', id);
    a5.setAttribute('data-for', id);
    a6.setAttribute('data-for', id);
    a7.setAttribute('data-for', id);
    a8.setAttribute('data-for', id);
    a9.setAttribute('data-for', id);
    h6.className = 'dropdown-header';
    a1.className = 'dropdown-item dropdown-item-checked checkedDropdown';
    a2.className = 'dropdown-item checkedDropdown';
    a3.className = 'dropdown-item checkedDropdown';
    a4.className = 'dropdown-item checkedDropdown';
    a5.className = 'dropdown-item checkedDropdown';
    a6.className = 'dropdown-item checkedDropdown';
    a7.className = 'dropdown-item checkedDropdown';
    a8.className = 'dropdown-item checkedDropdown';
    divider.className = 'dropdown-divider';
    a9.className = 'dropdown-item';

    div.appendChild(label);
    div.appendChild(row);
    row.appendChild(col1);
    col1.appendChild(textarea);
    row.appendChild(col2);
    col2.appendChild(dropdown);
    dropdown.appendChild(settings);
    settings.appendChild(cog);
    dropdown.appendChild(menu);
    menu.appendChild(h6);
    menu.appendChild(a1);
    menu.appendChild(a2);
    menu.appendChild(a3);
    menu.appendChild(a4);
    menu.appendChild(a5);
    menu.appendChild(a6);
    menu.appendChild(a7);
    menu.appendChild(a8);
    menu.appendChild(divider);
    menu.appendChild(a9);
    h6.appendChild(document.createTextNode('Rows'));
    a1.appendChild(document.createTextNode('3'));
    a2.appendChild(document.createTextNode('4'));
    a3.appendChild(document.createTextNode('5'));
    a4.appendChild(document.createTextNode('6'));
    a5.appendChild(document.createTextNode('7'));
    a6.appendChild(document.createTextNode('8'));
    a7.appendChild(document.createTextNode('9'));
    a8.appendChild(document.createTextNode('10'));
    a9.appendChild(document.createTextNode('Delete'));

    document.getElementById('contentBody' + taskId).appendChild(div);
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function guid() {
    function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}