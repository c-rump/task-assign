
$(function(){

	////////////// Text Box ///////////////////////

	$('#textBoxLabelEdit').on('input', function(){
		$('#previewLabel').text($(this).val());
	});

	$('#textBoxValue').on('input', function(){
		if($('input[name=textBoxValueType]:checked').val() == 'placeholder'){
			$('#previewTextBox').val('');
			$('#previewTextBox').attr("placeholder", $(this).val());
		}
		else{
			if($(this).val() == '')
				$('#previewTextBox').removeAttr('placeholder');
			$('#previewTextBox').val($(this).val());
		}
	});

	$('input[type=radio][name=textBoxValueType]').change(function(){
		if(this.value == 'placeholder'){
			$('#previewTextBox').val('');
			$('#previewTextBox').attr("placeholder", $('#textBoxValue').val());
		}
		else{
			if($(this).val() == '')
				$('#previewTextBox').removeAttr('placeholder');
			$('#previewTextBox').val($('#textBoxValue').val());
		}
	});

	$(document).on('click', '.dropdown-item', function(event) {
	  	event.preventDefault();
	  	$(this).toggleClass('dropdown-item-checked').siblings().removeClass('dropdown-item-checked');  
	});

	$('#textBoxTypeMenu a').on('click', function(){
		console.log('foo');
		$('#previewTextBox').prop('readonly', false);
		if(this.text == 'Password')
			$('#previewTextBox').attr('type', 'password');
		else if(this.text == 'Text')
			$('#previewTextBox').attr('type', 'text');
		else if(this.text == 'Email')
			$('#previewTextBox').attr('type', 'email');
		else if(this.text == 'URL')
			$('#previewTextBox').attr('type', 'url');
		else if(this.text == 'Telephone')
			$('#previewTextBox').attr('type', 'tel');
		else if(this.text == 'Read Only'){
			$('#previewTextBox').prop('readonly', true);
			$('#previewTextBox').attr('type', 'text');
		}
	});

	$('#textBoxHelperEdit').on('input', function(){
		$('#previewTextBoxHelp').text($(this).val());
	});

	///////////// Select ////////////////////

	$('#selectLabelEdit').on('input', function(){
		$('#selectPreviewLabel').text($(this).val());
	});

	$('#selectDefaultEdit').on('input', function(){
		$('#selectPreviewOption').text($(this).val());
	});

	$('#selectDefaultEditCheck').change(function(){
		if(!this.checked){
			$('#selectDefaultEdit').prop('disabled', true);
			$('#selectPreview option:selected').prop("selected", false);
			$('#selectPreview option[value="selectPreviewOption"]').remove();
		}
		else{
			$('#selectDefaultEdit').prop('disabled', false);
			var prev = document.getElementById('selectPreview');
			var option = document.createElement('option');
			option.id = 'selectPreviewOption';
			option.value = 'selectPreviewOption';
			option.selected = true;
			option.disabled = true;
			var optionText = document.createTextNode($('#selectDefaultEdit').val());
			option.appendChild(optionText);
			prev.appendChild(option);
		}
	});

	$('#selectAddOption').click(function(){
		if($('#selectEnterChoice').val().trim() != ''){
			var choices = document.getElementById('selectChoices');
			var prev = document.getElementById('selectPreview');

			var cOption = document.createElement('option');
			cOption.value = $('#selectEnterChoice').val();
			var cOptionText = document.createTextNode($('#selectEnterChoice').val());
			cOption.appendChild(cOptionText);

			var pOption = document.createElement('option');
			pOption.value = $('#selectEnterChoice').val();
			var pOptionText = document.createTextNode($('#selectEnterChoice').val());
			pOption.appendChild(pOptionText);	

			choices.appendChild(cOption);
			prev.appendChild(pOption);		

			$('#selectEnterChoice').val('');
		}
	});

	$('#selectChoices').change(function(){
		$('#selectChoiceRemove').prop('disabled', false);
	});

	$('#selectChoiceRemove').on('click', function(){
		$('#selectChoices option:selected').each(function(){
			$('#selectPreview option[value="' + $(this).val() + '"]').remove();
			$('#selectChoices option[value="' + $(this).val() + '"]').remove();
		})
	});

	
        
});

function buildSimpleTask(){
	var id = guid();
	var task = {type:'simple', div:'', id:id}
	var div = document.createElement('div');
	var row = document.createElement('div');
	row.className = 'row';
	var col = document.createElement('div');
	col.className = 'col';
	var h5  = document.createElement('h5');
	var txt = document.createTextNode($("#simpleTaskTitle").val());
	var p   = document.createElement('p');
	p.style.cssText = 'white-space: pre-wrap;';
	var ptxt= document.createTextNode($("#simpleTaskInstructions").val());

	div.appendChild(row);
	row.appendChild(col);
	col.appendChild(h5);
	h5.appendChild(txt);
	col.appendChild(p);
	p.appendChild(ptxt);
						
	task.div = div;
	/*task.html = '<div>' +
					'<div class="row">' +
						'<div class="col">' +
							'<h5>' + $("#simpleTaskTitle").val() + '</h5>' +
							'<p style="white-space: pre-wrap;">' + $("#simpleTaskInstructions").val() + '</p>' +
						'</div>' +
						'<div class="col-md-auto">' +
							'<span class="button-checkbox" id="' + id + '">' +
        						'<button type="button" class="btn btn-lg" data-color="info" disabled><span class="ml-1">Not Complete</span></button>' +
        						'<input type="checkbox" class="mr-1" style="display: none" checked />' +
      						'</span>' +
      					'</div>' +
      				'</div>' +
				'</div>';
	*/
	return task;
}

function resetSimpleTask(){
	$('#simpleTaskTitle').val('');
	$('#simpleTaskInstructions').val('');
}

function buildTaskWithText(){
	var id = guid();
	var task = {type:'taskWithTextBox', div:'', id:id}

	var div = document.createElement('div');
	var row = document.createElement('div');
	row.className = 'row';
	var col = document.createElement('div');
	col.className = 'col';
	var h5  = document.createElement('h5');
	var txt = document.createTextNode($("#taskWithTextTitle").val());
	var p   = document.createElement('p');
	p.style.cssText = 'white-space: pre-wrap;';
	var ptxt= document.createTextNode($("#taskWithTextInstructions").val());
	var area= document.createElement('textarea');
	area.className = 'form-control';
	area.rows = $('#taskWithTextLines').val();

	div.appendChild(row);
	row.appendChild(col);
	col.appendChild(h5);
	h5.appendChild(txt);
	col.appendChild(p);
	p.appendChild(ptxt);
	col.appendChild(area);

	task.div = div;
	return task;
}

function resetTaskWithText(){
	$('#taskWithTextTitle').val('');
	$('#taskWithTextInstructions').val('');
	$('#taskWithTextLines').val('3');
}

function buildTaskWithForm(){
	var id = guid();
	var task = {type:'taskWithForm', div:'', id:id, elements:[]}

	var div = document.createElement('div');
	var row = document.createElement('div');
	row.className = 'row';
	var col = document.createElement('div');
	col.className = 'col';
	var idDiv = document.createElement('div');
	idDiv.setAttribute('id', 'div' + id);
	var h5  = document.createElement('h5');
	var txt = document.createTextNode($("#taskWithFormTitle").val());
	var p   = document.createElement('p');
	p.style.cssText = 'white-space: pre-wrap;';
	var ptxt= document.createTextNode($("#taskWithFormInstructions").val());
	var bdiv= document.createElement('div');
	bdiv.className = 'text-center';
	var button = document.createElement('button');
	button.className = 'btn btn-primary addElementToForm mb-2';
	button.setAttribute('data-formid', id);
	button.appendChild(document.createTextNode('Add Form Element'));

	div.appendChild(row);
	row.appendChild(col);
	col.appendChild(idDiv);
	idDiv.appendChild(h5);
	h5.appendChild(txt);
	idDiv.appendChild(p);
	p.appendChild(ptxt);
	col.appendChild(bdiv);
	bdiv.appendChild(button);

	task.div = div;
	return task;
}

function resetTaskWithForm(){
	$('#taskWithFormTitle').val('');
	$('#taskWithFormInstructions').val('');
}



function buildTextBoxHTML(){
	var id = guid();
	var element = {type:'textBox', div:'', id:id}

	var div = document.createElement('div');
	div.className = 'form-group';
	var label = document.createElement('label');
	label.setAttribute('for', 'textbox' + id);
	var labelText = document.createTextNode($('#textBoxLabelEdit').val());
	var input = document.createElement('input');
	var attr = $('#previewTextBox').attr('readonly');

	if(typeof attr !== typeof undefined && attr !==false)
		input.setAttribute('readonly', true);
		
	input.type = $('#previewTextBox').attr('type');
	input.className = 'form-control';
	input.id = 'textbox' + id;
	input.setAttribute('aria-describedby', 'help' + id);
	input.setAttribute('placeholder', $('#previewTextBox').attr('placeholder'));
	input.value = $('#previewTextBox').val();
	var small = document.createElement('small');
	small.id = 'help' + id;
	small.className = 'form-text text-muted';
	var smallText = document.createTextNode($('#textBoxHelperEdit').val());

	div.appendChild(label);
	label.appendChild(labelText);
	div.appendChild(input);
	div.appendChild(small);
	small.appendChild(smallText);

	element.div = div;
	return element;
}

function resetTextBox(){
	$('#textBoxLabelEdit').val("Label");
	$('#previewLabel').text("Label");
	$('#textBoxValue').val("Placeholder text");
	$('#previewTextBox').val('');
	$('#previewTextBox').attr('placeholder', 'Placeholder text');
	$('#previewTextBox').attr('type', 'text');
	$('#textBoxTypeMenu a').removeClass('dropdown-item-checked');
	$('#textBoxTypeText').addClass('dropdown-item-checked');
	$('#textBoxHelperEdit').val("Helper text");
	$('#previewTextBoxHelp').text('Helper text');
	$('#textBoxPlaceholderOption').prop('checked', true);
	$('#previewTextBox').attr('readonly', false);
}

function buildSelectHTML(){
	var id = guid();
	var element = {type:'select', div:'', id:id}

	var div = document.createElement('div');
	div.className = 'form-group';
	var label = document.createElement('label');
	label.setAttribute('for', 'select' + id);
	var labelText = document.createTextNode($('#selectLabelEdit').val());
	var select = document.createElement('select');
	select.id = 'select' + id;
	select.className = 'form-control';

	div.appendChild(label);
	label.appendChild(labelText);
	div.appendChild(select);

	if($('#selectDefaultEditCheck').is(":checked")){
		var option = document.createElement('option');
		option.selected = true;
		option.disabled = true;
		var optionText = document.createTextNode($('#selectDefaultEdit').val());

		option.appendChild(optionText);
		select.appendChild(option);
	}
	var options = '';
	$('#selectChoices > option').each(function(){
		var option = document.createElement('option');
		option.value = this.value;
		var optionText = document.createTextNode(this.value);
		option.appendChild(optionText);
		select.appendChild(option);
	});
	
	element.div = div;
	return element;
}

function resetSelect(){
	$('#selectLabelEdit').val('Label');
	$('#selectPreviewLabel').text('Label');
	$('#selectPreview').find('option').remove().end()
		.append('<option id="selectPreviewOption" value="selectPreviewOption" selected disabled>Select an item...</option>').val('selectPreviewOption');
	$('#selectDefaultEdit').val('Select an item...');
	$('#selectEnterChoice').val('');
	$('#selectChoices').find('option').remove();
}

function getGroupNames(){
	$.ajax({
		type: 'GET',
		url: "http://127.0.0.1:8085/getGroupNames?orgid=" + localStorage.orgid,
		contentType: 'application/json; charset=utf-8',
		dataType: 'JSON',
		success: function(response){
			response.forEach(function(group, i){
				var option = document.createElement('option');
				option.value = group.id;
				var text = document.createTextNode(group.groupname);
				option.appendChild(text);
				document.getElementById('groups').appendChild(option);
			});
		},
		error: function (xhr, textStatus, errorThrown){
			console.log('error' + errorThrown);
			console.log('xhr', xhr);
			console.log(xhr.responseText);
		}
	});
}

function updateCheckButtons(id){
	$('#' + id).each(function () {

        // Settings
		var $widget = $(this),
	        $button = $widget.find('button'),
	        $buttonText = $widget.find('button span'),
	        $checkbox = $widget.find('input:checkbox'),
	        color = $button.data('color'),
	        settings = {
	            on: {
	                icon: 'far fa-square'
	            },
	            off: {
	                icon: 'far fa-check-square'
	            }
	        };

	    // Event Handlers
	    $button.on('click', function () {
	        $checkbox.prop('checked', !$checkbox.is(':checked'));
	        $checkbox.triggerHandler('change');

	        updateDisplay();
	    });
	    $checkbox.on('change', function () {
	        updateDisplay();
	    });

	    // Actions
	    function updateDisplay() {
	        var isChecked = $checkbox.is(':checked');

	        if(!isChecked){
	          $buttonText.text('Completed');
	        }
	        else
	          $buttonText.text('Not Complete');

	        // Set the button's state
	        $button.data('state', (isChecked) ? "on" : "off");

	        // Set the button's icon
	        $button.find('.state-icon')
	            .removeClass()
	            .addClass('state-icon ' + settings[$button.data('state')].icon);

	        // Update the button's color
	        if (isChecked) {
	            $button
	                .removeClass('btn-primary')
	                .addClass('btn-' + color + ' active');
	        }
	        else {
	            $button
	                .removeClass('btn-' + color + ' active')
	                .addClass('btn-primary');
	        }
	    }

	    // Initialization
	    function init() {

	        updateDisplay();

	        // Inject the icon if applicable
	        if ($button.find('.state-icon').length == 0) {
	            $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
	        }
	    }
	    init();
	});
}


 function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}


