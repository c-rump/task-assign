$(function(){

    $('body').on('click', '.btn-circle-inactive', function(){
        $('.btn-circle-active').removeClass('btn-circle-active').addClass('btn-circle-inactive');
        $(this).removeClass('btn-circle-inactive').addClass('btn-circle-active');
        $('.content-container').addClass('d-none');
        $('.content-container').removeClass('d-flex');
        
        var selected = $(this).next().text();
        if(selected == 'Assign Tasks'){
            $('#deliveryContainer').css('opacity', 0);
            $('#deliveryContainer').removeClass('d-none');
            animateChangeDiv($('#deliveryContainer'));
        }
        else if(selected == 'Running Tasks'){
            $('#runningContainer').css('opacity', 0);
            $('#runningContainer').removeClass('d-none');
            animateChangeDiv($('#runningContainer'));

            getRunningTasks();
        }
        
    });

    $('body').on('click', '.list-group-item', function(){
        if($(this).hasClass('expanded')){
            var item = $(this).next();
            var cur = item;
            while(item.hasClass('sub-item')){
                cur = item.next();
                item.remove();
                item = cur;
            }
            $(this).removeClass('expanded');
        }
        else{
            $(this).addClass('expanded')
            var id = $(this).attr('data-id');
            getTasksForList(id, this);
        }
        
    });

    $('#showByUser').tooltip({'trigger':'hover', 'placement':'top', 'title': 'Created by you'});
    $('#showByOrg').tooltip({'trigger':'hover', 'placement':'top', 'title': 'Created by your organization'});

    $('#taskListSelect').change(function(){
        reset();
        $.ajax({
            type: 'GET',
            url: "http://127.0.0.1:8085/getDeliveryInfo?orgid=" + localStorage.orgid + '&id=' + this.value,
            contentType: 'application/json; charset=utf-8',
            dataType: 'JSON',
            success: function(response){
                $('#deliveryMethodDiv').removeClass('d-none');
                if(typeof response.deliveryType !== typeof undefined)
                    $('#deliveryMethod').val(response.deliveryType).change();
                if(response.deliveryType == 'individual'){
                    $('#recipientList').empty();
                    var list = JSON.parse(response.userList);
                    for(var x = 0; x < list.length; x++){
                        var li = document.createElement('li');
                        li.className = 'list-group-item d-flex justify-content-between align-items-center list-group-item-action';
                        li.appendChild(document.createTextNode(list[x]));
                        var span = document.createElement('span');
                        span.className = 'fas fa-times removeButton';
                        li.appendChild(span);
                        document.getElementById('recipientList').appendChild(li);
                    }
                    $("input[name='howChoices'][value=" + response.how + "]").prop('checked', true);
                }
                else if(body.delivery.type == 'group'){
                    $('#groups').val(body.delivery.groupname);
                    $("input[name='whatChoices'][value=" + response.how + "]").prop('checked', true);
                }
                validate();
            },
            error: function (xhr, textStatus, errorThrown){
                console.log('error' + errorThrown);
                console.log('xhr', xhr);
                console.log(xhr.responseText);
            }
        });
    });

    $('.optionRadio').on('change', function(){
        if($(this).is(':checked')){
            if(this.value == 'user'){
                $('#showByOrg').removeClass('taskbar-radios-checked').addClass('taskbar-radios');
                $('#showByUser').removeClass('taskbar-radios').addClass('taskbar-radios-checked');
            }
            else{
                $('#showByUser').removeClass('taskbar-radios-checked').addClass('taskbar-radios');
                $('#showByOrg').removeClass('taskbar-radios').addClass('taskbar-radios-checked');
            }
        }
        getRunningTasks();
    });

    $("input[name='howChoices']").change(function(e){
        validate();
    });

    $("input[name='whatChoices']").change(function(e){
        validate();
    });

    $('#groups').change(function(){
        validate();
    });

    $('#deliveryMethod').on('change', function(){
        if(this.value == 'group'){
            $('.individualItems').addClass('d-none');
            $('#groupName').removeClass('d-none');
            $('#whatToDeliver').removeClass('d-none');
        }
        else if(this.value == 'individual'){
            $('.groupItems').addClass('d-none');
            $('#recipients').removeClass('d-none');
            $('#deliveryMethodChoices').removeClass('d-none');
        }
    });

    $('#recipientList').on('click', '.removeButton', function(e){
        e.preventDefault();
        $(this).parent().remove();
    });

    $(document).ready(function() {
        $(window).keydown(function(event){
            if(event.target.id == 'addRecipient' && event.keyCode == 13){
                event.preventDefault();

                var recipient = $('#addRecipient').val();

                if(recipient == '' || typeof reciepient != typeof undefined)
                    return false;
                
                if(validateUser(recipient))
                    addRecipientElement();
                
                return false;
            }
            else if(event.keyCode == 13) {
                //event.preventDefault();
                //return false;
            }
        });
    });

    function validateUser(recipient){
        recipient = recipient.toLowerCase();
        if(!(recipient in users)){
            return false;
        }
        if(!recipientAlreadyAdded(recipient))
            return true;
        return false;
    }

    $('#addRecipientButton').on('click', function(){
        var recipient = $('#addRecipient').val();
        if(recipient == '' || typeof reciepient != typeof undefined)
            return;
        if(validateUser(recipient))
                addRecipientElement();
    });

    function addRecipientElement(){
        var li = document.createElement('li');
        li.className = 'list-group-item d-flex justify-content-between align-items-center list-group-item-action';
        li.appendChild(document.createTextNode($('#addRecipient').val()));
        var span = document.createElement('span');
        span.className = 'fas fa-times removeButton';
        li.appendChild(span);
        document.getElementById('recipientList').appendChild(li);
        $('#addRecipient').val('');
        validate();
    }

    function recipientAlreadyAdded(recipient){
        var found = false;
        $('#recipientList').children().each(function(){
            if($(this).text().toLowerCase() == recipient){
                found = true;
            }
        });
        if(found)
            return true;
        return false;
    }

    $('#startTasks').click(function(){
        $('#startTasks').attr('disabled', true);
        var json = {
            id:$('#taskListSelect').val(),
            orgid:localStorage.orgid,
            type:'',
            owner:localStorage.userId,
            delivery:{
                users:[],
                choice:''
            }
        }
        if($('#deliveryMethod').val() == 'individual'){
            json.type = 'individual';
            $('#recipientList').children().each(function(){
                json.delivery.users.push(users[$(this).text().toLowerCase()]);
            });

            if(typeof $("input:radio[name='howChoices']:checked").val() !== typeof undefined && $("input:radio[name='howChoices']:checked").val() != ''){
                json.delivery.choice = $("input:radio[name='howChoices']:checked").val();
            }
        }

        $.ajax({
            type: 'POST',
            url: "http://127.0.0.1:8085/startTaskList",
            contentType: 'application/json; charset=utf-8',
            dataType: 'JSON',
            data: JSON.stringify(json),
            success: function(response){
                alert('Your tasks have been sent');
                $('#startTasks').attr('disabled', false);
            },
            error: function (xhr, textStatus, errorThrown){
                console.log('error' + errorThrown);
                console.log('xhr', xhr);
                console.log(xhr.responseText);
            }
        });
    });
});

function animateChangeDiv(div1){
    div1.animate({opacity: '1'}, 500, function(){
    });
}

function getRunningTasks(){
    var method = $("input:radio[name='displayOptions']:checked").val();
    $.ajax({
        type: 'GET',
        url: "http://127.0.0.1:8085/getActiveTaskLists?orgid=" + localStorage.orgid + '&user=' + localStorage.userId + '&method=' + method,
        contentType: 'application/json; charset=utf-8',
        dataType: 'JSON',
        success: function(response){
            buildRunningTasks(response);
        },
        error: function (xhr, textStatus, errorThrown){
            console.log('error' + errorThrown);
            console.log('xhr', xhr);
            console.log(xhr.responseText);
        }
    });
}

function buildRunningTasks(tasks){
    $('#taskLists').empty();
    for(var x = 0; x < tasks.length; x++){
        var div = document.createElement('div');
        div.className = "list-group-item list-group-item-action";
        div.setAttribute('data-id', tasks[x].id);
        div.setAttribute('data-title', tasks[x].taskListTitle);
        div.id = tasks[x].id;
        var row = document.createElement('div');
        row.className = 'row';
        var col = document.createElement('div');
        col.className = 'col-auto';
        var h1 = document.createElement('h1');
        var i = document.createElement('i');
        i.className = 'fas fa-clipboard-list';
        var col2 = document.createElement('div');
        col2.className = 'col';
        var div1 = document.createElement('div');
        div1.className = 'd-flex w-100 justify-content-between';
        var h5 = document.createElement('h5');
        h5.className = 'mb-1';
        var small = document.createElement('small');
        var div2 = document.createElement('div');
        div2.className = 'd-flex w-100 justify-content-between';
        var p = document.createElement('p');
        var p2= document.createElement('p');

        h5.appendChild(document.createTextNode(tasks[x].taskListName + ' - ' + tasks[x].taskListTitle));
        small.appendChild(document.createTextNode(getDuration(tasks[x].runningTime)))
        p.appendChild(document.createTextNode('Assigned by ' + tasks[x].username));
        p2.appendChild(document.createTextNode(tasks[x].completed + ' of ' + tasks[x].total + ' Tasks Completed'));

        div.appendChild(row);
        row.appendChild(col);
        col.appendChild(h1);
        h1.appendChild(i);
        row.appendChild(col2);
        col2.appendChild(div1);
        div1.appendChild(h5);
        div1.appendChild(small);
        col2.appendChild(div2);
        div2.appendChild(p);
        div2.appendChild(p2);

        document.getElementById('taskLists').appendChild(div);
    }
}

function getDuration(time) {
    let comp = time.split(":");
    
    var days = Math.floor(comp[0] / 24);
    if(days >= 1){
        if(days > 1)
            return days + ' days ago';
        return '1 day ago';
    }
    if(comp[0] > 0){
        if(comp[0] > 1) 
            return parseInt(comp[0], 10) + ' hours ago';
        return '1 hour ago';
    }
    if(comp[1] > 0){
        if(comp[1] > 1)
            return parseInt(comp[1], 10) + ' minutes ago';
        return '1 minute ago';
    }
    return 'Just now';
  }

function getTasksForList(id){
    $.ajax({
        type: 'GET',
        url: "http://127.0.0.1:8085/getTasksForActiveList?orgid=" + localStorage.orgid + '&taskListId=' + id,
        contentType: 'application/json; charset=utf-8',
        dataType: 'JSON',
        success: function(response){
            addTasksToList(response, id);
        },
        error: function (xhr, textStatus, errorThrown){
            console.log('error' + errorThrown);
            console.log('xhr', xhr);
            console.log(xhr.responseText);
        }
    });
}

function addTasksToList(tasks, parentId){
    var prev = null;
    for(var x = 0; x < tasks.length; x++){
        var div = document.createElement('div');
        div.setAttribute('data-for', tasks[x].id);
        div.className = "list-group-item list-group-item-action sub-item";
        var row = document.createElement('div');
        row.className = 'row';
        var col1 = document.createElement('div');
        col1.className = 'col ml-5';
        var numDiv = document.createElement('div');
        numDiv.className = 'row h-100';
        var num = document.createElement('p');
        num.className = 'sub-item-number my-auto';
        var col2 = document.createElement('div');
        col2.className = 'col-8';
        var titleDiv = document.createElement('div');
        titleDiv.className = 'row h-100';
        var title = document.createElement('h5');
        title.className = 'my-auto';
        var col3 = document.createElement('div');
        col3.className = 'col pr-0';
        var status = document.createElement('p');
        var user = document.createElement('p');

        num.appendChild(document.createTextNode("Task " + tasks[x].taskIndex));
        title.appendChild(document.createTextNode(tasks[x].title));
        user.appendChild(document.createTextNode(tasks[x].username));
        if(tasks[x].complete == 1)
            status.appendChild(document.createTextNode('Complete'));
        else
            status.appendChild(document.createTextNode('Incomplete'));

        div.appendChild(row);
        row.appendChild(col1);
        row.appendChild(col2);
        row.appendChild(col3);
        col1.appendChild(numDiv);
        numDiv.appendChild(num);
        col2.appendChild(titleDiv);
        titleDiv.appendChild(title);
        col3.appendChild(status);
        col3.appendChild(user);

        if(prev == null)
            document.getElementById('taskLists').insertBefore(div, document.getElementById(parentId).nextSibling);
        else
            document.getElementById('taskLists').insertBefore(div, prev.nextSibling);
        prev = div;
    }
}

function validate(){
    if(checkValues())
        $('#startTasks').attr('disabled', false);
    else
        $('#startTasks').attr('disabled', true);
}

function reset(){
    //$('#taskListSelect').val('default');
    $('#deliveryMethod').val('default');
    $('#groups').val('default');
    $('input:radio[name=howChoices]').each(function () { $(this).prop('checked', false); });
    $('input:radio[name=whatChoices]').each(function () { $(this).prop('checked', false); });
    $('#recipientList').empty();
    $('#deliveryMethodDiv').addClass('d-none');
    $('#groupName').addClass('d-none');
    $('#whatToDeliver').addClass('d-none');
    $('#recipients').addClass('d-none');
    $('#deliveryMethodChoices').addClass('d-none');
}

function checkValues(){
    if($('#taskListSelect').val() == 'default')
        return false;
    if($('#deliveryMethod').val() == 'individual'){
        if($('#recipientList').children().length == 0)
            return false;

        if(typeof $("input:radio[name='howChoices']:checked").val() === typeof undefined || $("input:radio[name='howChoices']:checked").val() == '')
            return false;
    }
    else if ($('#deliveryMethod').val() == 'group'){
        if($('#groups').val() == null)
            return false;

        if(typeof $("input:radio[name='whatChoices']:checked").val() === typeof undefined || $("input:radio[name='whatChoices']:checked").val() == '')
            return false;
    }
    else
        return false;
    return true;
}

//removed the ability to use email addresses for users. Keeping this here incase it changes back
/*function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}*/