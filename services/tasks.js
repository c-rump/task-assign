module.exports = function(app, connection){

    app.post('/saveTask', function(req, res){
        connection.query('INSERT INTO TaskLists (taskListName, taskListTitle, orgid) VALUES (?, ?, ?)', [req.body.taskListName, req.body.json.title, req.body.orgid], function(error, result, fields){
            if(error)
                res.send('db error');
            else{
                var groupId = -1;
                var userList = '';
                if(req.body.json.delivery.type == 'group')
                    groupId = req.body.json.delivery.groupname;
                else
                    userList = JSON.stringify(req.body.json.delivery.users);
                connection.query('INSERT INTO Delivery (orgid, deliveryType, how, groupId, userList, taskListId) ' + 
                                 'VALUES (?,?,?,?,?,?)', [req.body.orgid, req.body.json.delivery.type, req.body.json.delivery.choice, groupId, userList, result.insertId],
                                 function (err, rst, flds){
                    if(err)
                        res.send('db error');
                    else{
                        var promises = [];
                        for(var x = 0; x < req.body.json.tasks.length; x++){
                            promises.push(insertTaskSchema(req.body.orgid, result.insertId, req.body.json.tasks[x]));
                        }

                        Promise.all(promises).then(function(data){
                            res.send(JSON.stringify('success'));
                        }).catch(function(err){
                            res.send('db error');
                        });
                    }
                });
            }
        });
    });

    function insertTaskSchema(orgid, taskListId, json){
        return new Promise(function(resolve, reject){
            connection.query('INSERT INTO TaskSchema (orgid, taskListID, title, taskIndex, taskSchema) VALUES (?,?,?,?,?)', 
                              [orgid, taskListId, json.title, json.index, JSON.stringify(json)], function(error, result, fields){
                if(error)
                    return reject();
                return resolve();
            });
        });
    }

    app.get('/getTaskListNames', function(req, res){
        connection.query('SELECT taskListName, id FROM TaskLists WHERE orgid = ? ORDER BY taskListName', [req.query.orgid], function(error, result, fields){
            if(error)
                res.end('db error');
            res.send(JSON.stringify(result));
        });
    });

    app.get('/getDeliveryInfo', function(req, res){
        connection.query('SELECT deliveryType, how, groupId, userList FROM Delivery WHERE taskListId = ? and orgid = ?', [req.query.id, req.query.orgid], function(error, result, fields){
            if(error)
                res.end('db error');
            res.send(result[0]);
        });
    });

    app.post('/startTaskList', function(req, res){
        if(req.body.type == 'individual'){
            if(req.body.delivery.choice == 'entireList'){
                var promises = [];
                for(var x = 0; x < req.body.delivery.users.length; x++){
                    promises.push(startIndividualTask(req.body.orgid, req.body.id, req.body.owner, req.body.delivery.users[x]));
                }

                Promise.all(promises).then(function(data){
                    res.send(JSON.stringify('success'));
                }).catch(function(err){
                    res.send('db error');
                });
            }
        }
        else if(req.body.type == 'group'){

        }
    });

    function startIndividualTask(orgid, id, owner, user){
        return new Promise(function(resolve, reject){
            connection.query('CALL StartTaskList(?,?,?,?)', [orgid, id, owner, user], function(error, result, fields){
                if(error)
                    return reject();
                return resolve();
            });
        });
    }

    app.get('/getActiveTaskLists', function(req, res){
        connection.query('CALL GetActiveTasks(?,?,?)', [req.query.orgid, req.query.user, req.query.method], function(error, result, fields){
            if(error)
                res.end('db error');
            res.send(JSON.stringify(result[0]));
        })
    });

    app.get('/getTasksForActiveList', function(req, res){
        connection.query('CALL GetTasksForActiveList(?,?)', [req.query.orgid, req.query.taskListId], function(error, result, fields){
            if(error){
                res.end('db error');
            }
            if(typeof result !== typeof undefined)
                res.end(JSON.stringify(result[0]));
            else
                res.end(JSON.stringify('error'));
        })
    });

    app.get('/getRunningTasks', function(req, res){
        connection.query('SELECT atl.id, tl.taskListTitle,  TIMEDIFF(NOW(), atl.startTime) AS runningTime, CONCAT(u.firstName, " ", u.lastName) AS username, ' + 
                         'COUNT(a.complete) AS total, SUM(a.complete) As completed ' +
                         'FROM TaskLists tl, Users u, AssignedTaskList atl, AssignedTask a ' +
                         'WHERE tl.id = atl.taskListId ' +
                         'AND atl.ownerId = u.id ' +
                         'AND atl.id = a.assignedListId ' +
                         'AND a.assignedTo = ? AND atl.orgid = ?' +
                         'GROUP BY atl.id', [req.query.user, req.query.orgid], function(err, result, fields){
            if(err)
                res.end('db error');
            res.send(JSON.stringify(result));
        });
    });

    app.get('/getAssignedTasks', function(req, res){
        connection.query('SELECT a.id, a.taskValues, ts.taskIndex, ts.taskSchema, a.complete ' +
                         'FROM AssignedTask a, TaskSchema ts, AssignedTaskList atl ' +
                         'WHERE a.assignedListId = atl.id ' +
                         'AND ts.id = a.schemaId ' +
                         'AND atl.id = ? ' +
                         'AND a.assignedTo = ? ORDER BY taskIndex', [req.query.taskListId, req.query.user], function(error, result, fields){
            if(error)
                res.end('db error');
            res.send(JSON.stringify(result));
        })
    });

    app.post('/completeTask', function(req, res){
        connection.query('UPDATE AssignedTask SET complete = 1, taskValues = ?, completedTime = NOW() WHERE id = ?', [JSON.stringify(req.body.values), req.body.id], function(error, result, fields){
            if(error)
                res.senendd('db error');
            res.send(JSON.stringify('success'));
        })
    });

    app.post('/uncompleteTask', function(req, res){
        connection.query('UPDATE AssignedTask SET complete = 0, completedTime = null WHERE id = ?', [req.body.id], function(error, result, fields){
            if(error)
                res.end('db error');
            res.send(JSON.stringify('success'));
        });
    });

    app.post('/completeList', function(req, res){
        connection.query('Call CompleteTaskList(?, ?)', [req.body.taskListId, req.body.userId], function(error, result, fields){
            if(error)
                res.end('db error');
            res.send(JSON.stringify('success'));
        });
    });
    function notifiyListeners(){
        //keep a table of active listeners
        //if the table has the user, call this function
    }
}