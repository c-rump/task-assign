module.exports = function(app, connection){

    app.get('/getUsersAndGroups', function(req, res){
        connection.query('CALL GetUsersAndGroups(?)', [req.query.orgid], function(error, result, fields){
            if(error){
                res.end('db error');
                return;
            }
            res.send(JSON.stringify(result[0]));
        });
    });

    app.post('/removeUserFromGroup', function(req, res){
        connection.query('CALL RemoveUserFromGroup(?,?,?)', [req.body.orgid, req.body.groupid, req.body.userid], function(error, results, fields){
            if(error){
                res.end('db error');
                return;
            }
            res.send(JSON.stringify('success'));
        });
    });

    app.post('/addUserToGroup', function(req, res){
        connection.query('CALL AddUserToGroup(?,?,?)', [req.body.orgid, req.body.userid, req.body.groupid], function(error, results, fields){
            if(error){
                if(error.code == 'ER_DUP_ENTRY')
                    res.send('duplicate');
                else
                    res.end('db error');
                return;
            }
            res.send(JSON.stringify('success'));
        })
    });

    app.post('/deleteUser', function(req, res){
        connection.query('CALL DeleteUser(?,?)', [req.body.orgid, req.body.userid], function(error, results, fields){
            if(error){
                res.end('db error');
                return;
            }
            res.send(JSON.stringify('success'));
        });
    });

    app.post('/createUser', function(req, res){
        connection.query('CALL CreateUser(?,?)', [req.body.orgid, req.body.email], function(error, results, fields){
            if(error){
                if(error.code == 'ER_DUP_ENTRY')
                    res.send('duplicate');
                else
                    res.end('db error');
                return;
            }
            res.send(JSON.parse(JSON.stringify(results[0]))[0]);
        })
    });

    app.get('/getGroups', function(req, res){
        connection.query('CALL GetGroups(?)', [req.query.orgid], function(error, result, fields){
            if(error){
                res.end('db error');
                return;
            }
            res.send(JSON.stringify(result[0]));
        })
    });

    app.get('/getGroupNames', function(req, res){
        connection.query('CALL GetGroupNames(?)', [req.query.orgid], function(error, result, fields){
            if(error){
                res.end('db error');
                return;
            }
            res.send(JSON.stringify(result[0]));
        });
    });

    app.post('/createGroup', function(req, res){
        connection.query('CALL CreateGroup(?,?)', [req.body.orgid, req.body.groupname], function(error, results, fields){
            if(error){
                if(error.code == 'ER_DUP_ENTRY')
                    res.send('duplicate');
                else
                    res.end('db error');
                return;
            }
            res.send(JSON.parse(JSON.stringify(results[0]))[0]);
        })
    });

    app.post('/deleteGroup', function(req, res){
        connection.query('CALL DeleteGroup(?,?)', [req.body.orgid, req.body.groupid], function(error, results, fields){
            if(error){
                res.end('db error');
                return;
            }
            res.send(JSON.stringify('success'));
        });
    });

    /*app.post('/getUsers', function(req, res){
        connection.query('SELECT CONCAT(firstName, " ", lastName) as name, email, active, registered, id FROM Users WHERE orgid = ? ORDER BY firstName', 
            [req.body.orgid], function(error, results, fields){
                if(error)
                    res.end('db error');
            res.end(JSON.stringify(results));
        });
    });

    app.get('/getUsers', function(req, res){
        connection.query('SELECT CONCAT(firstName, " ", lastName) as name, id FROM Users WHERE orgId = ?', [req.query.orgid], function(error, results, fields){
                if(error)
                    res.send('db error');
            res.send(JSON.stringify(results));
        });
    });

    app.post('/addGroup', function(req, res){
        connection.query('INSERT INTO GroupDefs (groupname, orgid) VALUES (?, ?)', [req.body.groupname, req.body.orgid], function(error, result, fields){
            if(error)
                res.send('db error');
            insertGroupUsers(result.insertId, req.body.users).then(result =>{
                res.send(JSON.stringify(result));
            });
            
        });
    })

    async function insertGroupUsers(id, users){
        for(var x = 0; x < users.length; x++){
            await connection.query('INSERT INTO GroupUsers (group_id, user_id) VALUES (?, ?)', [id, users[x]], function(error, result, fields){
                if(error)
                    return 'db error';
            });
        }
        return 'success';
    }

    app.get('/getGroups', function(req, res){
      connection.query('SELECT groupname, user_id FROM GroupDefs, GroupUsers WHERE GroupDefs.id = GroupUsers.group_id and GroupDefs.orgid = ? ORDER BY groupname', 
        [req.query.orgid],
      function(error, results, fields){
        if(error)
            res.send('db error');
        res.send(JSON.stringify(results));
      });
    });

    app.get('/getGroupNames', function(req, res){
        connection.query('SELECT groupname, id FROM GroupDefs WHERE orgid = ? ORDER BY groupname', [req.query.orgid], function(error, result, fields){
            if(error)
                res.send('db error');
            res.send(JSON.stringify(result));
        })
    })
    */
}