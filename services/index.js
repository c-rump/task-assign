var express = require('express');
var mysql = require('mysql');
var app = express();
var Flow = require('../js/AutomationFlow');
var nodemailer = require('nodemailer');
const bcrypt = require('bcrypt');

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
  });

var transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: 'task.dev.notifyer@gmail.com',
		pass: 'Pancakes18'
	}
});

var connection = mysql.createConnection({
	host : 'localhost',
	user : 'automation',
	password : 'FrenchToast18',
	database : 'automation',
	multipleStatements: true
});
connection.connect();

app.use(express.json());

require('./people')(app, connection);
require('./tasks')(app, connection);

const saltRounds = 10;



app.post('/test', function(req, res){
	
	res.end("foo");
});

/////////// Login ///////////

app.post('/createLogin', function(req, res){
	bcrypt.hash(req.body.password, saltRounds, function(err, hash){
		if(err)
			res.end('error');
		connection.query('INSERT INTO Users (email, hash) VALUES (?, ?)', [req.body.email, hash], function(error, results, fields){
			if(error)
				res.end('db error');
			res.end('success');
		});
	});
})

app.post('/login', function(req, res){
	connection.query('SELECT firstName, lastName, hash FROM Users WHERE email = ?', [req.body.email], function(error, results, fields){
		if(error)
			res.end('db error');
		bcrypt.compare(req.body.password, results[0].hash, function(err, result) {
			// res == true
			if(result == true)
				res.end(JSON.stringify(results[0].firstName + ' ' + results[0].lastName));
			else
				res.end('fail');
		});
	});
})

/////////////////////////////

app.post('/saveTaskList', function(req, res){
	let taskList = req.body.taskList;
});

app.post('/createFlow', function(req, res){

	let flow = new Flow(req.body.name);
	console.log("name: ", flow.name);

	connection.query('INSERT INTO Flows (custID, name) VALUES (?, ?)', [req.body.custID, flow.name], function (error, results, fields){
		if(error)
			throw error;

	});

	res.end(JSON.stringify(flow.name));
});

app.post('/updateFlow', function(req, res){
	let flowId = req.body.flowId;
	let flow = req.body.flow;

	flow.forEach(function(step){
		console.log(step);
	});

	res.end(JSON.stringify('success'));
})

var server = app.listen(8085, function(){
	var host = server.address().address
	var port = server.address().port
   	console.log("Automation Server Listening")
})
